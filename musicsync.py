"""
    musicsync.py

    Provides a utility class around the Google Music API that allows for easy synching of playlists.
    Currently it will look at all the files already in the playlist and:
     Upload any missing files (and add them to the playlist)
     Add any files that are already uploaded but not in the online playlist
     Optionally remove any files from the playlist that are not in the local copy (does not delete
     files!)
     Uploads are done one by one followed by a playlist update for each file (rather than as a
     batch)
    It does not remove duplicate entries from playlists or handle multiple entries.

    TODO: Add optional duplicate remover

    API used: https://github.com/simon-weber/Unofficial-Google-Music-API
    Thanks to: Kevion Kwok and Simon Weber

    Use at your own risk - especially for existing playlists

    Free to use, reuse, copy, clone, etc

    Usage:
     ms = MusicSync()
     # Will prompt for Email and Password - if 2-factor auth is on you'll need to generate a one-
       time password
     ms.sync_playlist("c:/path/to/playlist.m3u")

     ms.delete_song("song_id")
"""
__author__ = "Tom Graham"
__email__ = "tom@sirwhite.com"


from gmusicapi import Mobileclient, Musicmanager
from gmusicapi.clients import OAUTH_FILEPATH
import mutagen
import json
import glob
import os
import time
import re
import codecs
from getpass import getpass
from httplib import BadStatusLine, CannotSendRequest
import urllib3
import certifi

MAX_UPLOAD_ATTEMPTS_PER_FILE = 3
MAX_CONNECTION_ERRORS_BEFORE_QUIT = 5
STANDARD_SLEEP = 5
MAX_SONGS_IN_PLAYLIST = 1000
LOCAL_OAUTH_FILE = './oauth.cred'


class MusicSync(object):

    def __init__(self, email=None, password=None):
        http = urllib3.PoolManager(
            cert_reqs='CERT_REQUIRED', # Force certificate check.
            ca_certs=certifi.where()   # Path to the Certifi bundle.
        )

        self.mm = Musicmanager()
        self.mc = Mobileclient()

        if not email:
            email = raw_input("Email: ")
        if not password:
            password = getpass()

        self.email = email
        self.password = password
        self.logged_in = self.auth()

    def auth(self):
        self.logged_in = self.mc.login(self.email, self.password, Mobileclient.FROM_MAC_ADDRESS)
        if not self.logged_in:
            print "Login failed..."
            exit()

        print "Logged in as %s" % self.email

        if not os.path.isfile(OAUTH_FILEPATH):
            print "First time login. Please follow the instructions below:"
            self.mm.perform_oauth()
        self.logged_in = self.mm.login()
        if not self.logged_in:
            print "OAuth failed... try deleting your %s file and trying again." % OAUTH_FILEPATH
            exit()
        print "Authenticated"

    def song_name(self, filename):
        return os.path.splitext(filename)[0]

    def playlist_does_not_exist(self, playlist_title, goog_playlists):
        return playlist_title not in [g['name'] for g in goog_playlists]

    def sync_playlists(self, folder, remove_missing=False):
        os.chdir(folder)

        playlist_titles = map(self.song_name, glob.glob("*.m3u"))

        if len(playlist_titles) > 0:
            print "Fetching songs from Google..."
            all_songs = self.mc.get_all_songs()
            print "Got %d songs." % len(all_songs)

            print "Fetching playlists from Google..."
            goog_playlists = self.mc.get_all_user_playlist_contents()
            print "Got %d playlists." % len(goog_playlists)

            print "Creating new empty playlists..."
            playlists_to_create = [pl for pl in playlist_titles if self.playlist_does_not_exist(pl, goog_playlists)]

            print "Creating %d playlists." % len(playlists_to_create)

            if len(playlists_to_create) > 0:
                for newPlaylist in playlists_to_create:
                    print "  Creating playlist: %s" % newPlaylist
                    self.mc.create_playlist(newPlaylist)
                goog_playlists = self.mc.get_all_user_playlist_contents()
                print "Now have %d playlists." % len(goog_playlists)

            for playlist_title in playlist_titles:
                pc_songs = self.get_files_from_playlist(os.path.join(folder, playlist_title) + ".m3u")
                playlist = next((playlist for playlist in goog_playlists if (playlist['name'] == playlist_title)), None)
                if playlist is not None:
                    self.update_playlist(playlist, pc_songs, all_songs, remove_missing)

    def update_playlist(self, playlist, pc_songs, all_songs, remove_missing):
        print "Syncing playlist: %s" % playlist['name']
        plid = playlist['id']
        goog_songs = playlist['tracks']
        print "  %d songs already in Google Music playlist" % len(goog_songs)
        print "  %d songs in local playlist" % len(pc_songs)
        if len(pc_songs) > MAX_SONGS_IN_PLAYLIST:
            print "    Google music doesn't allow more than %d songs in a playlist..." % MAX_SONGS_IN_PLAYLIST
            print "    Will only attempt to sync the first %d songs." % MAX_SONGS_IN_PLAYLIST
            del pc_songs[MAX_SONGS_IN_PLAYLIST:]

        new_songs = [s for s in pc_songs if not self.file_already_in_list(s, goog_songs, all_songs)]
        songs_to_add = [self.find_or_upload_song(s, all_songs) for s in new_songs]

        added_songs = len(self.mc.add_songs_to_playlist(plid, songs_to_add))
        failed_songs = len(songs_to_add) - added_songs
        removed_songs = 0
        if remove_missing:
            songs_to_remove = [s['id'] for s in goog_songs if self.song_not_in_files(s, pc_songs, all_songs)]
            print "  %d songs to remove" % len(songs_to_remove)
            removed_songs = len(self.mc.remove_entries_from_playlist(songs_to_remove))
        existing_songs = len(goog_songs) - removed_songs

        print "%d songs unmodified" % existing_songs
        print "%d songs added" % added_songs
        print "%d songs failed" % failed_songs
        print "%d songs removed" % removed_songs
        print "Ended: %s" % playlist['name']

    def find_or_upload_song(self, pc_song, all_songs):
        goog_song = self.find_song(pc_song, all_songs)
        if goog_song is None:
            song_id = self.upload_song(pc_song)
        else:
            song_id = goog_song['id']
        return song_id

    def upload_song(self, pc_song):
        print "   adding: %s" % os.path.basename(pc_song)
        attempts = 0
        result = []
        while not result and attempts < MAX_UPLOAD_ATTEMPTS_PER_FILE:
            time.sleep(.2) # Don't spam the server too fast...
            print "  uploading... (may take a while)"
            attempts += 1
            try:
                result = self.mm.upload(fn)
            except (BadStatusLine, CannotSendRequest):
                # Bail out if we're getting too many disconnects
                if fatal_count >= MAX_CONNECTION_ERRORS_BEFORE_QUIT:
                   print "Too many disconnections - quitting. Please try running the script again."
                   exit()

                print "Connection Error -- Reattempting login"
                fatal_count += 1
                self.mc.logout()
                self.mm.logout()
                result = []
                time.sleep(STANDARD_SLEEP)

            except:
                result = []
                time.sleep(STANDARD_SLEEP)

        try:
            if result[0]:
                song_id = result[0].itervalues().next()
            else:
                song_id = result[1].itervalues().next()
            print "  upload complete [%s]" % song_id
        except:
            print "  upload failed - skipping"

    def get_files_from_playlist(self, filename):
        files = []
        f = codecs.open(filename, encoding='utf-8')
        for line in f:
            line = line.rstrip().replace(u'\ufeff',u'')
            if line == "" or line[0] == "#":
                continue
            path  = os.path.abspath(self.get_platform_path(line))
            if not os.path.exists(path):
                print "  file not found: %s" % line
                continue
            files.append(path)
        f.close()
        return files

    def song_not_in_files(self, goog_song, pc_songs, all_songs):
        song = self.get_song_by_trackid(goog_song['trackId'], all_songs)
        for pc_song in pc_songs:
            tag = self.get_id3_tag(pc_song)
            if self.song_compare(song, tag['title'], tag['artist'], tag['album'], tag['track']):
                return False
        return True

    def file_already_in_list(self, filename, goog_songs, all_songs):
        tag = self.get_id3_tag(filename)
        for goog_song in goog_songs:
            song = self.get_song_by_trackid(goog_song['trackId'], all_songs)
            if self.song_compare(song, tag['title'], tag['artist'], tag['album'], tag['track']):
                return True
        return False

    def get_id3_tag(self, filename):
        data = mutagen.File(filename, easy=True)
        r = {}
        if 'title' not in data:
            title = os.path.splitext(os.path.basename(filename))[0]
            print '  found song with no ID3 title, setting using filename:'
            print '    %s' % title
            print '    (please note - the id3 format used (v2.4) is invisible to windows)'
            data['title'] = [title]
            data.save()
        r['title'] = data['title'][0]
        r['track'] = int(data['tracknumber'][0].split('/')[0]) if 'tracknumber' in data else 0
        # If there is no track, try and get a track number off the front of the file... since thats
        # what google seems to do...
        # Not sure how google expects it to be formatted, for now this is a best guess
        if r['track'] == 0:
            m = re.match("(\d+) ", os.path.basename(filename))
            if m:
                r['track'] = int(m.group(0))
        r['artist'] = data['artist'][0] if 'artist' in data else ''
        r['album'] = data['album'][0] if 'album' in data else ''
        return r

    def get_song_by_trackid(self, track_id, all_songs):
        return next((song for song in all_songs if (song['id'] == track_id)), None)

    def find_song(self, filename, all_songs):
        tag = self.get_id3_tag(filename)

        # NOTE - diagnostic print here to check results if you're creating duplicates
        print "  [%s][%s][%s][%s]" % (tag['title'], tag['artist'], tag['album'], tag['track'])
        for s in all_songs:
            if self.song_compare(s, tag['title'], tag['artist'], tag['album'], tag['track']):
                print "  %s matches %s" % (s['title'], tag['title'])
                return s
        print "  No matches for %s" % tag['title']
        return None

    def song_compare(self, song, title, artist, album, track):
        if song is not None:
            if 'trackNumber' not in song:
                song['trackNumber'] = 0
            return  song['title'].lower() == title.lower() and\
                    song['artist'].lower() == artist.lower() and\
                    song['album'].lower() == album.lower() and\
                    song['trackNumber'] == track
        return False

    def get_platform_path(self, full_path):
        # Try to avoid messing with the path if possible
        if os.sep == '/' and '\\' not in full_path:
            return full_path
        if os.sep == '\\' and '\\' in full_path:
            return full_path
        if '\\' not in full_path:
            return full_path
        return os.path.normpath(full_path.replace('\\', '/'))